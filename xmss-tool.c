/***
 * XMSS tool
 *
 * This is a demo tool with which you can sign and verify messages through XMSS.
 * It is based on the XMSS reference implementation.
 * Currently this tool is capable of:
 *
 *  - Generating an XMSS key pair.
 *  - Sign a message with an XMSS private key.
 *  - Verify an XMSS signature with the public key.
 *  - Display info about a private key.
 *
 * Author: Tom Vrancken  (dev@tomvrancken.nl)
 * (C) Copyright 2019
 */

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "xmss/params.h"
#include "xmss/xmss.h"
#include "xmss/utils.h"

/*
 * Generates a XMSS public/private key pair
 *
 * @paramstr: A parameter string that specifies the XMSS algorithm parameters.
 * @outfile: A filepath/filename string that specifies the file where the key pair must be written to.
 */
int gen_keypair(const char* paramstr, const char* outfile) {
    xmss_params params;
    uint32_t oid = 0;
    int res;
    FILE* fp_pk;
    FILE* fp_sk;
    char pk_file[80];
    char sk_file[80];

    /* First convert the given parameter string
     * that specifies what kind of key we need to
     * generate to an object id (oid).
     */
    xmss_str_to_oid(&oid, paramstr);

    /* Then initialize the XMSS algorithm parameters
     * based on the param set oid.
     */
    res = xmss_parse_oid(&params, oid);

    if (res < 0) {
        fprintf(stderr, "Error initializing algorithm parameters.\n");
        return res;
    }

    printf("Generate XMSS key pair...\n");

    // Late init because params.*_bytes need to be initialized
    unsigned char pk[XMSS_OID_LEN + params.pk_bytes];
    unsigned char sk[XMSS_OID_LEN + params.sk_bytes];

    // Generate the key pair
    xmss_keypair(pk, sk, oid);

    // Write the public key to an output file
    strcpy(pk_file, outfile);  // unsafe!
    strcat(pk_file, ".pubkey"); // unsafe!

    fp_pk = fopen(pk_file, "w");
    fwrite(pk, 1, XMSS_OID_LEN + params.pk_bytes, fp_pk);
    fclose(fp_pk);

    // Write the private key to an output file
    strcpy(sk_file, outfile); // unsafe!
    strcat(sk_file, ".privkey"); // unsafe!

    fp_sk = fopen(sk_file, "w");
    fwrite(sk, 1, XMSS_OID_LEN + params.sk_bytes, fp_sk);
    fclose(fp_sk);

    return 0;
}

/*
 * Signs a message with the given secret key
 *
 * @sk_file: A string specifying the filename with the secret key.
 * @msg_file: A string specifying the filename that contains the message to be signed.
 * @out_file: A string specifying the filename that will be generated with the signed message.
 */
int sign_message(const char* sk_file, const char* msg_file, const char* out_file) {
    xmss_params params;
    uint32_t oid = 0;
    int res;
    int msg_len = 0;
    FILE* fp_sk;
    FILE* fp_msg;
    FILE* fp_out;
    uint8_t oid_buf[XMSS_OID_LEN];

    // First open the private key
    fp_sk = fopen(sk_file, "r+b");

    if (fp_sk == NULL) {
        fprintf(stderr, "Could not open private key file.\n");

        return -1;
    }

    // Then extract the oid
    fread(&oid_buf, 1, XMSS_OID_LEN, fp_sk);
    oid = (uint32_t)bytes_to_ull(oid_buf, XMSS_OID_LEN);

    // Then initialize the algorithm parameters
    res = xmss_parse_oid(&params, oid);

    if (res < 0) {
        fprintf(stderr, "Error initializing algorithm parameters from priv key oid.\n");
        fclose(fp_sk);

        return res;
    }

    // Read the secret key (including the OID part)
    unsigned char sk[XMSS_OID_LEN + params.sk_bytes];

    rewind(fp_sk);
    fread(sk, 1, XMSS_OID_LEN + params.sk_bytes, fp_sk);

    // Read the message that we want to sign
    fp_msg = fopen(msg_file, "rb");

    if (fp_msg == NULL) {
        fprintf(stderr, "Error opening message file.\n");

        return -1;
    }

    // Find out the message length.
    fseek(fp_msg, 0, SEEK_END);
    msg_len = ftell(fp_msg);

    unsigned char* msg        = malloc(msg_len);
    unsigned char* signed_msg = malloc(params.sig_bytes + msg_len);
    unsigned long long signed_msg_len;

    // Read the message
    rewind(fp_msg);
    fread(msg, 1, msg_len, fp_msg);
    fclose(fp_msg);

    // Sign the message and update the private key
    xmss_sign(sk, signed_msg, &signed_msg_len, msg, msg_len);

    // Write the updated private key to the priv key file
    fseek(fp_sk, -((long int)params.sk_bytes), SEEK_CUR);
    fwrite(sk + XMSS_OID_LEN, 1, params.sk_bytes, fp_sk);
    fclose(fp_sk);

    // Write the signed message to the output file
    fp_out = fopen(out_file, "w");

    if (fp_out == NULL) {
        fprintf(stderr, "Could not open output file.\n");

        return -1;
    }

    fwrite(signed_msg, 1, signed_msg_len, fp_out);
    fclose(fp_out);

    // Cleanup
    free(msg);
    free(signed_msg);

    return 0;
}

/*
 * Verifies an XMSS signature with the given public key
 *
 * @pk_file: A string specifying the filename with the public key.
 * @msg_file: A string specifying the filename that contains the signature.
 */
int verify_signature(const char* pk_file, const char* signed_msg_file) {
    xmss_params params;
    uint32_t oid = 0;
    int res;
    int signed_msg_len = 0;
    FILE* fp_pk;
    FILE* fp_signed_msg;
    uint8_t oid_buf[XMSS_OID_LEN];

    // First open the public key file
    fp_pk = fopen(pk_file, "rb");

    if (fp_pk == NULL) {
        fprintf(stderr, "Error opening key file containing public key.\n");

        return -1;
    }

    // Then extract the oid
    fread(&oid_buf, 1, XMSS_OID_LEN, fp_pk);
    oid = (uint32_t)bytes_to_ull(oid_buf, XMSS_OID_LEN);

    // Then initialize the algorithm parameters
    res = xmss_parse_oid(&params, oid);

    if (res < 0) {
        fprintf(stderr, "Error initializing algorithm parameters from pub key oid.\n");
        fclose(fp_pk);

        return res;
    }

    // Read the public key (including the OID part)
    unsigned char pk[XMSS_OID_LEN + params.pk_bytes];

    rewind(fp_pk);
    fread(pk, 1, XMSS_OID_LEN + params.pk_bytes, fp_pk);
    fclose(fp_pk);

    // Read the message that we want to sign
    fp_signed_msg = fopen(signed_msg_file, "rb");

    if (fp_signed_msg == NULL) {
        fprintf(stderr, "Error opening signed message file.\n");

        return -1;
    }

    // Find out the message length.
    fseek(fp_signed_msg, 0, SEEK_END);
    signed_msg_len = ftell(fp_signed_msg);

    unsigned char* msg        = malloc(signed_msg_len);
    unsigned char* signed_msg = malloc(signed_msg_len);
    unsigned long long msg_len;

    // Read the message
    rewind(fp_signed_msg);
    fread(signed_msg, 1, signed_msg_len, fp_signed_msg);
    fclose(fp_signed_msg);

    // Verify the signature
    res = xmss_sign_open(msg, &msg_len, signed_msg, signed_msg_len, pk);

    if (res) {
        printf("Verification failed!\n");
    } else {
        printf("Verification succeeded.\n");
    }

    // Cleanup
    free(msg);
    free(signed_msg);

    return res;
}

/*
 * Display info about a given secret key
 *
 * Displays then algorithm parameters string, the current key index and the number
 * of remaining signatures.
 *
 * @sk_file: A string specifying the filename with the secret key.
 */
int display_key_info(const char* sk_file) {
    xmss_params params;
    FILE* fp_sk;
    uint8_t oid_buf[XMSS_OID_LEN];
    uint8_t idx_buf[XMSS_IDX_LEN];
    uint32_t oid = 0;
    uint32_t key_idx = 0;
    uint32_t key_quotum = 0;
    char paramstr[20];
    int res;

    // Open the secret key file
    fp_sk = fopen(sk_file, "rb");

    if (fp_sk == NULL) {
        fprintf(stderr, "Could not open private key file.\n");

        return -1;
    }

    // Read the key's OID
    fread(&oid_buf, 1, XMSS_OID_LEN, fp_sk);
    oid = (uint32_t)bytes_to_ull(oid_buf, XMSS_OID_LEN);

    // Initialize the algorithm parameters corresponding to the OID (need for the tree height)
    res = xmss_parse_oid(&params, oid);

    if (res < 0) {
        fprintf(stderr, "Error initializing algorithm parameters from priv key oid.\n");
        fclose(fp_sk);

        return res;
    }

    // Convert the OID to a human readable string
    res = xmss_oid_to_str(oid, paramstr);

    if (res < 0) {
        fprintf(stderr, "Error converting oid to string. Unknown OID given.\n");
        fclose(fp_sk);

        return res;
    }

    // Read the key's index
    fread(&idx_buf, 1, XMSS_IDX_LEN, fp_sk);
    key_idx = (uint32_t)bytes_to_ull(idx_buf, XMSS_IDX_LEN);

    // Calculate the key's remaining quotum
    key_quotum = pow(2, params.tree_height) - key_idx;

    // Display key's param set identifier and index
    printf("Key parameters ID: %s\n", paramstr);
    printf("Key current index: %d\n", key_idx);
    printf("This private key can be used for %d remaining signatures.\n", key_quotum);

    fclose(fp_sk);

    return 0;
}

/* Demo application for XMSS signatures
 *
 * Command line arguments:
 * -g: generate a key pair
 * -s: sign a message
 * -v: verify a signature
 * -k: specify a key file, pub key for verification, priv key for signing
 * -m: specify a message file, plain text for signing, signed file for verifying
 * -o: specify an output file, for key pair, for signed message
 * -i: display private key info
 */
int main(int argc, char** argv) {
    int opt;
    char* paramstr = NULL;
    char* out_file = NULL;
    char* key_file = NULL;
    char* msg_file = NULL;
    bool do_keygen = false;
    bool do_sign   = false;
    bool do_verify = false;
    bool display_info = false;

    // First parse the command line options
    while((opt = getopt(argc, argv, "g:k:svo:m:i")) != -1) {

        switch (opt) {
            case 'g': // Generate a key pair
                do_keygen = true;
                paramstr  = optarg;
                break;
            case 's': // Sign a message
                do_sign = true;
                break;
            case 'v': // Verify a signature
                do_verify = true;
                break;
            case 'k': // Specify a key file. Pub key when verifying, secret key when signing.
                key_file = optarg;
                break;
            case 'm': // Specify a message file for signing or verifying.
                msg_file = optarg;
                break;
            case 'o':
                out_file = optarg;
                break;
            case 'i':
                display_info = true;
                break;
        }

    }


    if (do_keygen) { // Generate an XMSS key pair

        if (out_file == NULL) {
            printf("Error: output filename required. Please provide a filename via '-o' flag.\n");

            return -1;
        }

        return gen_keypair(paramstr, out_file);

    } else if (do_sign) { // Sign a message

        if (key_file == NULL || msg_file == NULL || out_file == NULL) {
            printf("Error: key file, message file and output file required.\n");

            return -1;
        }

        return sign_message(key_file, msg_file, out_file);

    } else if (do_verify) { // Verify a signature

        if (key_file == NULL || msg_file == NULL) {
            printf("Error: key file and signed message file required.\n");

            return -1;
        }

        return verify_signature(key_file, msg_file);

    } else if (display_info) {
        if (key_file == NULL) {
            printf("Error: a private key file must be specified.\n");

            return -1;
        }

        return display_key_info(key_file);

    }

    return 0;
}