README
------

This is a demo tool with which you can sign and verify messages through XMSS.
It is based on the XMSS reference implementation.
Currently this tool is capable of:

* Generating an XMSS key pair.
* Sign a message with an XMSS private key.
* Verify an XMSS signature with the public key.
* Display info about a private key.

### Important:
Currently this tool is experimental and unsafe for production environments. It
does not do any checks on input lengths and is therefore prone to buffer
overflows.

### Usage:
Current cli arguments:

* **-g**: Generates an XMSS key pair. Must be used in combination with the `-o` flag.
* **-s**: Signs an input message with an XMSS private key. Must be used in combination
with the `-k`, `-m` and `-o` flags.
* **-v**: Verifies an XMSS signature with a public key. Must be used in combination
with the `-k` and `-m` flags.
* **-k**: Specifies a key file to be used. This can be either a private key, used in
combination with `-s`, or a public key, used in combination with `-v`.
* **-m**: Specifies an input filename for the file that contains the message that needs
to be signed (in combination with `-s`), or the input filename that hold the signature
that needs to be verified (in combination with `-v`).
* **-o**: Specifies an output filename. When used in conjuction with `-g` you must only
specify a basename without file extension.
* **-i**: Displays info about a private key. Must be used with `-k`.

### Example parameters strings:
Below are some example parameter strings that can be given as input for `-g`:

* XMSS-SHA2\_10_256
* XMSS-SHA2\_16_256
* XMSS-SHA2\_10_512
* XMSS-SHAKE\_10_256

For a full list check the XMSS specification (<https://tools.ietf.org/html/rfc8391>).
